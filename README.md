# cervezalocal

Template de Wordpress compatible con Woocommerce desarrollado para tienda virtual cervezalocal.co


v0.1 Prototipo Inicial 14DIC2018

Funcionalidades
-Lista de productos disponibles por categorías
-Manejo de inventario disponible
-Cupones de oferta
-Generación de ordenes de pedido
-Notificación de pedido por correo electrónico
-Seguimiento Google Analytics y Search Control
-SEO para páginas, productos, categorías e imágenes
