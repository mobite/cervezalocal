</div>
<div class="firma-footer">
  <!-- <img src="<?php echo get_template_directory_uri() ?>/img/logo-cervezaLocal-60.png" alt=""> -->
  <p><i class="fab fa-whatsapp"></i> +57 318 294 5940  ·  pedidos@cervezalocal.co</p>
  <div class="redes-div">
      <a href="https://www.facebook.com/cervezalocal.co/" target="_blank"><i class="fab fa-facebook-square fa-lg"></i></a>
      <a href="https://www.instagram.com/cervezalocal.co/" target="_blank"><i class="fab fa-instagram fa-lg"></i></a>
  </div>
  <p style="margin-top:15px; text-align: center; font-size:10px; color:#666">© 2018  <a style="color:#666" href="https://www.mobitech.co" target="_blank">Mobitech Colombia SAS</a>.</p>

</div>


<!-- <div class="firma-div" style="background-color:#f9f9f9; border-top:1px solid #ccc; padding:20px 5% 30px; margin-top:0px;">
  <p style="float:left; font-size:10px; color:#444; margin:0">© 2018 Galería Colombia. Todos los derechos reservados.</p>
 <a href="https://www.mobitech.co" target="_blank"> <p style="float:right; font-size:10px; color:#444">Mobitech</p></a>
</div> -->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <!-- <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script> -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>



    <!-- <script type="text/javascript" src="slick/slick.min.js"></script> -->
    <script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>

    <!-- sweetalert -->
    <script src="<?php echo get_template_directory_uri() ?>/js/sweetalert2.all.min.js"></script>
    <!-- <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script> -->

<!--    <script type="text/javascript" src="https://favoriteposts.com/wp-content/plugins/favorites/assets/js/favorites.min.js?ver=2.1.6"></script> -->
   <!-- <script src="<?php echo get_template_directory_uri() ?>/js/favorites.min.js"></script> -->
   <!-- <script src="<?php echo get_template_directory_uri() ?>/js/share.js"></script> -->
<!--    <script src="<?php echo get_template_directory_uri() ?>/js/jquery.usp.core.js"></script>-->

    <script type="text/javascript">
    // var buscar = "<?php echo $_GET['buscar'] ?>";
  //  alert(buscar);

  //https://api.flickr.com/services/rest/?method=flickr.groups.pools.getPhotos&api_key=d41b5fdac033592f420af8d8e4804272&group_id=1358024%40N21&extras=geo%2C+tags&per_page=500&page=1&format=json&nojsoncallback=1

  // function openNav() {
  //     document.getElementById("sidenav").style.width = "250px";
  // }
  //
  // function closeNav() {
  //     document.getElementById("sidenav").style.width = "0";
  // }
    var $ = jQuery.noConflict();
    $(document).ready(function() {


        // bind change event to select
        $('.search-bar select').on('change', function () {
            var url = $(this).val(); // get selected value
            if (url) { // require a URL
                window.location = url; // redirect
            }
            return false;
        });


    $('.destacados').slick({
      dots: false,
      infinite: true,
      arrows: false,
      speed: 300,
      slidesToShow: 3,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 980,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 500,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
    });

    $('.nav-destacados .next').on('click', function(){
      $('.destacados').slick('slickNext');
    })
    $('.nav-destacados .prev').on('click', function(){
      $('.destacados').slick('slickPrev');
    })

      setTimeout(function(){
      $('#cargando-text').addClass('hidden');
      $('.container').addClass('loaded');
      $('.woocommerce-product-gallery').css('opacity', '1');
      $('#container-f').addClass('loaded');
      $('#container2').addClass('loaded');
      $('#myCarousel').addClass('loaded');
    }, 300);
    setTimeout(function(){
      $('.nav-home').css('opacity', '1');
    }, 1000);

      $('.flip-card').on('click', function(){
        $('.close-card').css('visibility', 'hidden')
        $(this).parent().find('.close-card').css('visibility', 'visible')
        $('.flip-card-inner').removeClass('flipiar')
        $(this).find('.flip-card-inner').addClass('flipiar')
        // $(this).find('.flip-card-inner').css('transform', 'rotateY(180deg)')
        // $(this).find('.flip-card-inner').removeClass('flipiar-off')
        // $(this).find('img').css('max-width', '200px')
        // alert('click')
      })

      $('.close-card').on('click', function(){
        $(this).parent().find('.flip-card .flip-card-inner').removeClass('flipiar')
        $(this).css('visibility', 'hidden')

        // alert('click')
        // $(this).parents('.flip-card-inner').css('transform', 'rotateY(0deg)')
        // $(this).parents('.flip-card-inner').addClass('flipiar-off')
        // $(this).parents('.flip-card-inner').removeClass('flipiar')
        // $('.flip-card-inner').css('transform', 'rotateY(0deg) !important')
        // $(this).find('img').css('max-width', '200px')
        // alert('click')
      })
      $('.contenedor-producto-loop').on('click', function(){
        // $(this).find('img').css('max-height', '200px')
        // $(this).find('img').css('max-width', '200px')
        // alert('click')
      })
      $('.comprar-loop').on('click', function(){
          alert($(this).attr('data-product_id'))

      })
      $('.sumar-cantidad-producto').on('click', function(){
          var cant = $(this).parent().find('.cantidad-producto').val()
          // $('.cantidad-producto').val(parseInt(cant)+1)
          // $(this).parent().find('a').attr('data-quantity', parseInt(cant)+1)
          $(this).parent().find('.form-comprar-loop form .form-cantidad').attr('value', parseInt(cant)+1)
          $(this).parent().find('.cantidad-producto').val(parseInt(cant)+1)
      })
      $('.restar-cantidad-producto').on('click', function(){
          var cant = $(this).parent().find('.cantidad-producto').val()
          // $('.cantidad-producto').val(parseInt(cant)+1)
          if (cant > 1){
          $(this).parent().find('.form-comprar-loop form .form-cantidad').attr('value', parseInt(cant)-1)
          $(this).parent().find('.cantidad-producto').val(parseInt(cant)-1)
        }
      })



      $('#menu-hamburguesa').on('click', function (e) {
        $("#sidenav").css('width', '250px');
        setTimeout(function(){
          $(".sidenav-wrapper").removeClass('hidden');
        }, 400);
      });
      $('#close-menu-hamburguesa').on('click', function (e) {
        $(".sidenav-wrapper").addClass('hidden');
        setTimeout(function(){
        $("#sidenav").css('width', '0px');
      }, 50);
      });
      $('.select-menu-buscar').on('click', function (e) {
        $('.menu-buscar').toggle()
        // $('.menu-buscar').css("display", "block")
      });
      $('.select-menu-categoria').on('click', function (e) {
        $('.menu-categoria').css("display", "block")
      });
      $('.close-menu-categoria').on('click', function (e) {
        $('.menu-categoria').css("display", "none")
      });




      // $('.contenedor-producto-loop').mouseout(function(){
      //   $(this).find('img').css('max-height', 'none')
      //   $(this).find('img').css('max-width', 'none')
      //   // alert('click')
      // })


      $('#usp_add-another').click(function(event) {
    		event.preventDefault();
    		var $this = jQuery(this);
    		var $newInput = $this.parent().find('input:visible:last').clone().val('');
    		$this.before($newInput);
    	});



    $('#tab-product a:first').tab('show')
      $('#tab-product a').on('click', function (e) {
    e.preventDefault()
    $(this).tab('show')
  });
  $('.input-text').addClass('form-control');
  $('#pa_tipo-de-marco').addClass('form-control');
  $('.country_select').addClass('form-control');
  $('.button').addClass('btn btn-primary');
  // $('.woocommerce-info').addClass('alert alert-secondary');


  $('.btn-enlace').on('click', function (e) {
    e.preventDefault();
    var direccion = $(this).attr('href');
    var direccion_blank = $(this).attr('target');
//    $(this).tab('show')
//  location.href = direccion;
//  alert(direccion_blank);
  if (direccion_blank=="_blank") {
    window.open(direccion,'_blank')
  } else {
    location.href = direccion;
  }
    });



  $('[data-fancybox]').fancybox({
    buttons : ['close']
  });
	$("[comunidad-flickr]").fancybox({
    });
  $("[historia]").fancybox({
      });
    $( "#variations_form" ).change(function() {
//        var direccion = document.getElementById('variations_form').attr('data-product_variations').value;
        var color_variacion = $("#pa_tipo-de-marco").val();
        var variaciones = JSON.parse($(this).attr('data-product_variations'));
        var contador_variaciones = variaciones.length;
        for (i = 0; i < contador_variaciones; i++) {
          if(color_variacion==variaciones[i]['attributes']['attribute_pa_tipo-de-marco']){
            var url_imagen = variaciones[i]['image']['src'];
            var url_link = variaciones[i]['image']['full_src'];
            var imagen1 = '<?php echo get_the_post_thumbnail_url( url_imagen, 'medium'); ?>';
            $( "#imagen-variacion" ).find( "img" ).attr('srcset', url_imagen);
            $( "#imagen-variacion" ).attr('href', url_link);
//            console.log(color_variacion);
//            console.log(url_imagen);
          }
        }

        console.log(variaciones);
//        document.getElementById("imagen-variable").append( imagen1 );


    });
  });


    </script>


<?php if ( is_home() ) {
  if($_COOKIE["TestCookie"]) {
  } else {
    ?>
    <!-- <script>
    swal(
      'Good job!',
      'You clicked the button!',
      'success'
    )
    </script> -->
    <?php
  }
 }
?>



  </body>
</html>
