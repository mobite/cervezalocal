<?php get_header() ?>
<div class="wrapper-container">
  <div class="container" style="padding: 30px 20px;">
    <?php woocommerce_breadcrumb(); ?>
    <?php woocommerce_content(); ?>
  </div>
</div>
<?php get_footer() ?>
