<?php get_header() ?>

<div class="wrapper-container">
<?php wc_print_notices(); ?>

    <div class="wrapper-destacados row">
      <h3>Disponible en botella</h3>
      <div class="nav-destacados ml-auto">
        <div class="prev">
          <i class="far fa-arrow-alt-circle-left fa-3x"></i>
        </div>
        <div class="next">
          <i class="far fa-arrow-alt-circle-right fa-3x"></i>
        </div>
      </div>
    </div>

  <div class="container" style="padding:0 0 20px;">


    <div class="destacados">

          <?php
          $tax_query[] = array(
            'taxonomy' => 'product_visibility',
            'field'    => 'name',
            'terms'    => 'featured',
            'operator' => 'IN', // or 'NOT IN' to exclude feature products
        );
          $args = array(
                  'posts_per_page' => '12',
                  'post_type' => 'product',
                  'orderby' => 'menu_order',
                  'tax_query' => $tax_query
              );


          $query = new WP_Query( $args );


          if( $query->have_posts()) : while( $query->have_posts() ) : $query->the_post();
          global $product;
          ?>
          <div class="" style="padding-top: 20px;">
          <a class="close-card" style="z-index: 10;position: relative;"><i class="fa fa-times fa-2x"></i></a>
          <div class="flip-card">
            <div class="flip-card-inner">
              <div class="flip-card-front" style="margin-top: -20px;">
                <?php the_post_thumbnail('shop_catalog', [ 'alt' => esc_html ( get_the_title() ) ]); ?>
                <h4 style="color:#fff; text-align:center; margin:0"><?php the_title(); ?></h4>
                <!-- <?php if ( get_post_meta( get_the_ID(), 'contenido', true ) ) : ?>
                  <h6 class="contenido"><?php echo get_post_meta(get_the_ID(), 'contenido', true); ?></h6>
                <?php endif; ?> -->
                <?php if ( get_post_meta( get_the_ID(), 'fabricante', true ) ) : ?>
                  <h6 class="fabricante"><?php echo get_post_meta(get_the_ID(), 'fabricante', true); ?></h6>
                <?php endif; ?>
                <?php if ( get_post_meta( get_the_ID(), 'tipo_cerveza', true ) ) : ?>
                  <h6 class="tipo_cerveza"><?php echo get_post_meta(get_the_ID(), 'tipo_cerveza', true); ?></h6>
                <?php endif; ?>

                <!-- <?php if ( get_post_meta( get_the_ID(), 'fabricante', true ) ) : ?>
                  <h6 class="fabricante"><?php echo get_post_meta(get_the_ID(), 'fabricante', true); ?></h6>
                <?php endif; ?> -->
                <h3 style="">$<?php echo $product->get_price() ?></h3>
              </div>
              <div class="flip-card-back" style="margin-top: -20px;">
                <p><?php the_post_thumbnail('thumbnail'); ?></p>
                  <h4 style="color:#fff; text-align:center;margin:0"><?php the_title(); ?></h4>
                  <?php if ( get_post_meta( get_the_ID(), 'grado_alcohol', true ) ) : ?>
                    <h6 class="grado_alcohol"><?php echo get_post_meta(get_the_ID(), 'grado_alcohol', true); ?> alc/vol</h6>
                  <?php endif; ?>


                  <h3 style="margin:0">$<?php echo $product->get_price() ?></h3>
                  <!-- <div style="padding-bottom:15px"><?php echo $product->post->post_excerpt ?></div> -->
                  <?php if ( $product->get_stock_status() == "outofstock" ) : ?>
                  <p style="font-size:0.85em; color:red; margin: 4px 0 0;">Agotado</p>
                <?php else: ?>
                  <button class="restar-cantidad-producto"><i class="fas fa-minus-circle fa-2x"></i></button>
                  <input type="text" value="1" class="cantidad-producto">
                  <button class="sumar-cantidad-producto"><i class="fas fa-plus-circle fa-2x"></i></button>
                <div class="form-comprar-loop">
                  <form class="" action="" method="post">
                    <input class="form-cantidad" type="hidden" name="quantity" value="1" title="Qty">
                    <input type="hidden" name="add-to-cart" value="<?php echo get_the_ID(); ?>" />
                    <button type="submit" class="single_add_to_cart_button button <?php if ( $product->get_stock_status() == "outofstock" ) : echo "disabled"; endif; ?>">Comprar</button>
                  </form>
                  <?php if ( $product->get_stock_quantity() ) : ?>
                  <p style="color:#aaa; font-size: 0.85em; margin: 4px 0 0;"><?php if ( $product->get_stock_quantity() == 1 ) : echo "1 disponible"; else: echo $product->get_stock_quantity(). " disponibles"; endif; ?></p>
                  <?php endif; ?>
                </div>

                  <?php endif; ?>

              </div>
          </div>
        </div>
      </div>

        <?php endwhile; endif; ?>


    </div>




    <div class="">

      <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

        <?php get_the_title() ?>
        <?php the_content() ?>

      <?php endwhile; else : ?>
        <!-- <p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p> -->
      <?php endif; ?>

    </div>

  </div>

</div>

<?php get_footer() ?>
