<?php get_header() ?>
<div class="wrapper-container">
  <div class="container" style="padding:30px 20px;">

  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

  <?php get_the_title() ?>
  <?php the_content() ?>

  <?php endwhile; else : ?>
  <p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
  <?php endif; ?>
  </div>
</div>
<?php get_footer() ?>
