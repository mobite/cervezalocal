<!doctype html>
<html lang="es">
  <head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-92907010-10"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-92907010-10');
    </script>

    <!-- config tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php wp_head(); ?>


    <!-- Site icon -->
    <?php wp_site_icon(); ?>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

    <!-- fontawesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.css" />

    <!-- fonts -->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/font-awesome.min.css">
    <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>

    <!-- slick -->
    <!-- <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/> -->
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() ?>/css/slick-theme.css"/>


    <!-- main css -->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/main.css">

      <!-- animate.css -->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/animate.css">


    <!-- <link rel="stylesheet" type="text/css" href="slick/slick.css"/> -->
    <?php
    $value = 'true';
    setcookie("TestCookie", $value, time() + 30);
    ?>

    <!-- fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700" rel="stylesheet">
  </head>
  <body>
    <div class="wrapper-site">
        <div class="nav-pantalla<?php if ( !is_home() ) { echo " nav-internas"; } ?>">
          <div class="" style="padding: 15px 20px;">
            <div class="menu-button-div">
              <a id="menu-hamburguesa"><i class="fa fa-bars fa-lg" aria-hidden="true"></i></a>
            </div>
            <div class="logo-div" style="display: inline-block;">
              <?php if ( function_exists( 'the_custom_logo' ) ) {
                $custom_logo_id = get_theme_mod( 'custom_logo' );
                $logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
                if ( has_custom_logo() ) {
                        echo '<a href="'. get_home_url() .'"><img src="'. esc_url( $logo[0] ) .'"></a>';
                } else {
                        echo '<h1>'. get_bloginfo( 'name' ) .'</h1>';
                }
              } ?>
            </div>
            <div class="topmenu-div" style="">
            <?php wp_nav_menu( array(
              'theme_location' => 'top-menu',
              'container_class' => 'topmenu-div',
              'container' => false,
              'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>' )
            ); ?>
            <a class="link-carrito" href="<?php global $woocommerce; echo $woocommerce->cart->get_cart_url() ?>"><i class="fa fa-shopping-basket" aria-hidden="true"></i><span class="badge badge-dark" style="margin-left:3px;font-size: 10px;color: #000;background-color: #ddd;"><?php $count = WC()->cart->cart_contents_count; echo $count; ?></span></a>
          </div>

          <?php if ( is_home() ) { ?>
            <div class="contenido-cabezote">
              <p class="titulo animated fadeInUp delay-0.5s">Ancheta Cervecera</p>
              <p class="animated fadeInUp delay-0.8s">Lleva 6 botellas de nuestra selección de Cervezas Artesanales de temporada.</p>
              <a href="<?php echo get_home_url() ?>/producto/ancheta-cervecera/"><button class="btn btn-danger  animated fadeIn delay-1s">Compra a domicilio</button></a>
            </div>

          <?php  } ?>

          <div class="btn-ciudad">
            <span><i class="fas fa-map-marker-alt"></i> Cali</span>
          </div>
         </div>
         <div class="search-bar">
           <div class="btn-group btn-categorias" role="group">
            <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Elige tus cervezas
            </button>
            <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
              <a class="dropdown-item" href="<?php echo get_home_url() ?>/comprar/cervezas-artesanales/">Cervezas Artesanales</a>
              <a class="dropdown-item" href="<?php echo get_home_url() ?>/comprar/cervezas-nacionales/">Cervezas Nacionales</a>
            </div>
          </div>
           <span class="select-menu-buscar" style="float:right; font-size:1.6em;margin-right:20px"><i class="fas fa-search"></i></span>
         </div>
       </div>

       <div class="menu-buscar">
         <form role="search" method="get" class="woocommerce-product-search" action="<?php echo esc_url( home_url( '/' ) ); ?>">
         	<!-- <label class="screen-reader-text" for="woocommerce-product-search-field-<?php echo isset( $index ) ? absint( $index ) : 0; ?>"><?php esc_html_e( 'Search for:', 'woocommerce' ); ?></label> -->
         	<input type="search" id="woocommerce-product-search-field-<?php echo isset( $index ) ? absint( $index ) : 0; ?>" class="search-field" placeholder="<?php echo esc_attr__( 'Search products&hellip;', 'woocommerce' ); ?>" value="<?php echo get_search_query(); ?>" name="s" />
         	<!-- <button type="submit" value="<?php echo esc_attr_x( 'Search', 'submit button', 'woocommerce' ); ?>"><?php echo esc_html_x( 'Search', 'submit button', 'woocommerce' ); ?></button> -->
         	<input type="hidden" name="post_type" value="product" />
         </form>
       </div>
       <div id="sidenav" class="sidenav">
         <div class="sidenav-wrapper hidden">
        <a id="close-menu-hamburguesa" href="javascript:void(0)" class="closebtn">&times;</a>
        <a class="link-sidenav" href="<?php echo get_home_url() ?>">Inicio</a>
        <a class="link-sidenav" href="<?php echo get_home_url() ?>/comprar/promos/">Promos</a>
        <a class="link-sidenav" href="<?php echo get_home_url() ?>/comprar/packs/">Packs</a>
        <div class="logo-sidenav">
          <img src="<?php echo get_template_directory_uri() ?>/img/logoCervezaLocal-negro150.png" alt="">
          <p>+57 318 294 5940</p>
          <p>pedidos@cervezalocal.co</p>
          <div class="redes-div">
              <a href="https://www.facebook.com/cervezalocal.co/" target="_blank"><i class="fab fa-facebook-square fa-lg"></i></a>
              <a href="https://www.instagram.com/cervezalocal.co/" target="_blank"><i class="fab fa-instagram fa-lg"></i></a>
          </div>
        </div>
      </div>
      </div>
